# Clappy: lightweight command line arguments

## Unmaintained

This library is not currently maintained. The pico-args crate is filling this niche very nicely.

https://crates.io/crates/pico-args

## Rationale

Clappy is a very small Rust library for parsing command line arguments for small programs with simple needs, where the excellent clap.rs is too much overhead, and processings std::env::args() directly is too much of a hassle.

The design objectives are:
- Small code base (should stay below 1000 LOC excluding tests)
- No dependencies other than the standard library
- Straightforward API (modelled after the "Immediate UI" style)

## Example

```rust
use clappy::Clappy;

fn main() {
	let mut args = Clappy::new("Display command line inputs"); 
		// args is mutable because it remembers the options for validity checking
	let do_capitalize = args.flag('c', "capitalize", "Capitalize arguments");
	let inputs = args.inputs();
	args.validate_or_exit(); // prints help and exits if it finds an error
	// finished with arguments, use them
	for s in inputs {
		println!("{}", if do_capitalize {s.to_ascii_uppercase()} else {s.to_string()});
	}
}
```
