const HEADLINE: &'static str = "Test program";

use crate::Clappy;

fn test<'a>(cl: &'a [&str]) -> Clappy<'a> {
	Clappy::from_array(HEADLINE, cl)
}

#[test]
fn test_flag() {
	let mut subj = test(&["-y"]);
	assert!(subj.flag('y', "yes", "Flag sample"));
	assert!(!subj.flag('v', "verbose", "Another flag"));
	subj = test(&["-y", "hello"]);
	assert!(subj.flag('y', "yes", "Flag sample"));
	subj = test(&["-a", "-y"]);
	assert!(subj.flag('y', "yes", "Flag sample"));
}

#[test] 
fn test_string() {
	let mut subj = test(&["-n", "foo", "-m"]);
	assert_eq!(subj.string('n', "name", "A string option"), Some("foo"));
	assert_eq!(subj.string('m', "name_alt", "Another string option"), None);
	assert_eq!(subj.string('p', "path", "Another string option"), None);
	subj = test(&["--name=foo", "--name_alt"]);
	assert_eq!(subj.string('n', "name", "A string option"), Some("foo"));
	assert_eq!(subj.string('m', "name_alt", "Another string option"), None);
	assert_eq!(subj.string('p', "path", "Another string option"), None);
}

#[test]
fn test_number() {
	let mut subj = test(&["-i", "123", "-j"]);
	assert_eq!(subj.number('i', "index", "Index number"), Some(123));
	assert_eq!(subj.number('j', "index_j", "Another index"), None);
	assert_eq!(subj.number('k', "index_k", "K value"), None);
	subj = test(&["--index=123", "--index_j"]);
	assert_eq!(subj.number('i', "index", "Index number"), Some(123));
	assert_eq!(subj.number('j', "index_j", "Another index"), None);
	assert_eq!(subj.number('k', "index_k", "K value"), None);
}

