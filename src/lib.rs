#[cfg(test)]
mod tests;

use std::env::args;
use std::process::exit;
use std::collections::HashSet;

const EXIT_CODE: i32 = -1; // process exit code when validation fails

struct OptSpec<'a> {
	value: &'static str,
	short_opt: char,
	long_opt: &'a str,
	help: &'a str,
}

struct OptIter<'a> {	
	arguments: &'a Vec<String>,
	index: usize,
}

enum Opt<'a> {
	Solo(&'a str),
	Valued(&'a str, &'a str)
}

pub struct Clappy<'a> {
	headline: String,
	args: Vec<String>,
	options: Vec<OptSpec<'a>>, 
}

impl OptSpec<'_> {
	fn is_consumer(&self) -> bool {
		self.value != "FLAG"
	}
}


impl<'a> Opt<'a> {
	fn is_solo(&self) -> bool {
		match self { Opt::Solo(_) => true, _ => false}
	}
	fn matches(&self, short:char, long: &str) -> bool {
		let opt = match self {Opt::Solo(o)=> o, Opt::Valued(o,_) => o};
		option_contains(&opt.chars().nth(0), &short) || opt == &long
	}

}

impl<'a> OptIter<'a> {
	fn new(arguments: &'a Vec<String>) -> OptIter<'a> {
		OptIter{arguments, index: 1}
	}

	fn find(&mut self, short: char, long: &str) -> Option<Opt<'a>> {
		while let Some(opt) = self.next() {
			if opt.matches(short, long) {
				return Some(opt);
			}
		}
		None
	}
}

impl<'a> Iterator for OptIter<'a> {
	type Item = Opt<'a>;
	fn next(&mut self) -> Option<Opt<'a>> {
		if self.index >= self.arguments.len() {
			None
		} else {
			let opt = &self.arguments[self.index];
			self.index += 1;
			if starts_with_and_some(opt, "--") {
				Some(match opt.find('=') {
					Some(eq_at) => Opt::Valued(&opt[2..eq_at], &opt[eq_at+1..]),
					None => Opt::Solo(&opt[2..])
				})
			} else if starts_with_and_some(opt, "-") {
				let opt1 = &opt[1..];
				Some(if self.index >= self.arguments.len() { // last flag
					Opt::Solo(opt1)
				} else {
					let value_candidate = &self.arguments[self.index];
					if value_candidate.starts_with("-") { // next is flag
						Opt::Solo(opt1)
					}
					else { // consume value
						self.index += 1; // not strictly necessary						
						Opt::Valued(opt1, value_candidate)
					}
				})
			} else {
				self.next() // look ahead for a flag
			}
		}
	}
}


impl<'a> Clappy<'a> {
	pub fn new(headline: &str) -> Self {
		// FIXME borrow headline
		Self{headline: String::from(headline), args: args().collect(), options: Vec::new()}
	}

	/// Initialize from command line parameters (without program name)
	pub fn from_array(headline: &str, strs: &[&str]) -> Self {
		let mut args = Vec::new();
		args.push(String::from("_")); // dummy program
		args.extend(strs.iter().map(|s| String::from(*s)));
		Self{headline: String::from(headline), args, options: Vec::new()}
	}
	/// Is the flag set?
	pub fn flag(&mut self, short_opt: char, long_opt: &'a str, help: &'a str) -> bool {
		assert!(valid_opt(short_opt, long_opt));
		self.options.push(OptSpec{value: "FLAG", short_opt, long_opt, help});
		match OptIter::new(&self.args).find(short_opt, long_opt) {
			Some(Opt::Solo(_)) | Some(Opt::Valued(_,_)) => true,
			None => false
		}
	}

	/// Number option (i64)
	pub fn number(&mut self, short_opt: char, long_opt: &'a str, help: &'a str) -> Option<i64> {
		assert!(valid_opt(short_opt, long_opt));
		self.options.push(OptSpec{value: "NUMBER", short_opt, long_opt, help});
		match OptIter::new(&self.args).find(short_opt, long_opt) {
			Some(Opt::Valued(_,s)) => s.parse().ok(),
			_ => None
		}
	}
	/// String option
	pub fn string<'b>(&'b mut self, short_opt: char, long_opt: &'a str, help: &'a str) -> Option<&'b str> {
		assert!(valid_opt(short_opt, long_opt));
		self.options.push(OptSpec{value: "STRING", short_opt, long_opt, help});
		match OptIter::new(&self.args).find(short_opt, long_opt) {
			Some(Opt::Valued(_,s)) => Some(s),
			_ => None
		}
	}
	/// Get value of a given type converted using From trait
//	pub fn value<T>(&mut self, short_opt: char, long_opt: &str, help: &str) -> Option<T> {
//		self.options.push(OptSpec{value: "VALUE", short_opt, long_opt, help});
//		match OptIter::new().find(short_opt, long_opt) {
//			Some(Opt::Valued(_,s) => s.parse::<T>(), 
//			None => None
//		}
//	}

// TODO
//	required_number
//	required_string
	/// Non option trailing arguments
	pub fn inputs(&self) -> Vec<&str> {
		// FIXME more functional implementation?
		let consumers: HashSet<char> = self.options.iter().filter(|o| o.is_consumer()).map(|o| o.short_opt).collect();
		let mut skip_next = false;		
		let mut result = Vec::new();
		let mut iter = self.args.iter();
		let _0 = iter.next(); // skip program name
		for a in iter {
			if a.starts_with('-') && !a.starts_with("--") { // skip short option, remember parameters
				result.clear();
				match a.chars().nth(1) {
					Some(optchar) => if consumers.contains(&optchar) {skip_next = true},
					None => ()
				}
			}
			else if a.starts_with("--") {} // skip long option
			else {
				if skip_next {
					skip_next = false;
				} else {
					result.push(a.as_str());
				}
			}
		}
		result
	}

	/// Validate using options value that have previously been requested that:
	/// - options with parameters have one
	/// - no unknown options
	/// - all values are consumed
	pub fn valid(&self) -> bool {
		let names = OptNames::new(
				self.options.iter().map(|o| o.short_opt).collect(), 
				self.options.iter().map(|o| o.long_opt).collect());
		let mut validated = true;
		// check all options on command line are known
		for opt in OptIter::new(&self.args) { // FIXME use iter
			validated = validated && match opt {
				Opt::Solo(nm) => names.contains(nm),
				Opt::Valued(nm,_) => names.contains(nm),
			}
		}
		// check consumers have an option
		let consumers: Vec<&OptSpec> = self.options.iter().filter(|o| o.is_consumer()).collect();
		let c_names = OptNames::new(
				consumers.iter().map(|o| o.short_opt).collect(),
				consumers.iter().map(|o| o.long_opt).collect());
		for opt in OptIter::new(&self.args) {
			validated = validated && match opt {
				Opt::Solo(nm) => !c_names.contains(nm),
				_ => true
			}
		}
		// FIXME 
		// check all values are consumed
		// 1. no dangling value between options
		// 2. inputs are consumed or absent
		validated
	}

	/// Print help on stderr
	pub fn help(&self) {
		eprintln!("{}", self.headline);
		for desc in &self.options {
			if desc.is_consumer() {
				eprintln!("-{1} {0}|--{2}={0} {3}", desc.value, desc.short_opt, desc.long_opt, desc.help);
			} else {
				eprintln!("-{}|--{} {}", desc.short_opt, desc.long_opt, desc.help);
			}
		}
	}

	pub fn validate_or_exit(&self) {
		if !self.valid() {
			self.help();
			exit(EXIT_CODE);
		}
	}
}

struct OptNames<'a> {
	shorts: HashSet<char>,
	longs: HashSet<&'a str>,
}

impl<'a> OptNames<'a> {
	fn new(shorts: HashSet<char>, longs: HashSet<&'a str>) -> Self {
		Self{shorts, longs}
	}
	fn contains(&self, s: &str) -> bool {
		if s.len() == 1 {
			self.shorts.contains(&s.chars().next().unwrap())
		} else {
			self.longs.contains(s)
		}
	}
}
// option name/letter cannot start with a dash
pub fn valid_opt(short_opt: char, long_opt: &str) -> bool {
	short_opt != '-' && !long_opt.starts_with('-') && long_opt.len() > 1
}

// Pending Option::contains in standard lib
fn option_contains<U,T>(opt: &Option<T>, x: &U) -> bool where U: PartialEq<T> {
    match opt {
        Some(y) => x == y,
        None => false,
    }
}

// str.starts_with with a tail
fn starts_with_and_some(subject: &str, start: &str) -> bool {
	subject.starts_with(start) && subject.len() > start.len()
}
