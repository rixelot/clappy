use clappy::Clappy;

fn main() {
	let mut args = Clappy::new("Display command line inputs"); 
		// args is mutable because it remembers the options for validity checking
	let do_capitalize = args.flag('c', "capitalize", "Capitalize arguments");
	let inputs = args.inputs();
	args.validate_or_exit(); // prints help and exits if it finds an error
	// finished with arguments, use them
	for s in inputs {
		println!("{}", if do_capitalize {s.to_ascii_uppercase()} else {s.to_string()});
	}
}
